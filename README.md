# PHP test 

* Data: https://admin.b2b-carmarket.com//test/project
* Write a parser for provided data, so you get a valid associative array of all the rows
* Create appropriate database table and write all the rows into it, be careful there could be a lot of rows
* Generate fake Names and Surnames for all of the Buyers in separated table, one for each buyer.
* Based on data provided from url, write CLI scripts:
  * For getting best selling model per client
  * Script that adds new record to the table and write test for adding to the table
  * For getting best selling model in three months in a row
* *BONUS POINTS: Everything should be up and running with single command (except program installation (Vagrant, Docker, etc)*


# Submission of the task:
 * Add me permissions to your repo - so i can check the code.


